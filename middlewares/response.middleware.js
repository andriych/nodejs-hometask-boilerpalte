const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err) {
        res.send(JSON.stringify({error: true, message: res.err.message}));
    }

    if(res.data){
        res.status(200).send(JSON.stringify(res.data));
    }

    next();
}

exports.responseMiddleware = responseMiddleware;