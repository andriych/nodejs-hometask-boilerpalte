const { fighter } = require('../models/fighter');

const isName = (value) => {
    if (!value) return false;
    if (!value.trim()) return false;
    return true;
}

const isPower = (value) => {
    if (!value) return false;
    if (value != 0 && !(value > 1 && value < 100)) return false;
    return true;
}

const isDefense = (value) => {
    if (!value) return false;
    if (!(value > 1 && value < 10)) return false;
    return true;
}

const isHealth = (value) => {
    if (!value) return false;
    if (!(value > 80 && value < 120)) return false;
    return true;
}

const checkOnCreateModel = (objOne, objTwo) => {
    const objOneKeys = Object.keys({...objOne, id: "", health:100}).sort();
    const objTwoKeys = Object.keys({...objTwo, id: "", health:100}).sort();
    return  JSON.stringify(objOneKeys) === JSON.stringify(objTwoKeys);
}

const checkOnUpdateModel = (pattern, toCompare) => {
    const patternKeys = Object.keys(pattern);
    const toCompareKeys = Object.keys(toCompare);

    if(patternKeys.length === 0 || toCompareKeys.length === 0) return false;

    const otherKeys = toCompareKeys.filter(a => !patternKeys.some(b => a === b))
    return !otherKeys.length;
}

const createFighterValid = (req, res, next) => {
    try {
        if(!req.body){
            throw new Error('Request body must be not null')
        }

        const correctProps = checkOnCreateModel(fighter, req.body);
        
        if (!correctProps || req.body.hasOwnProperty("id")){
            throw new Error("Fighter entity to update isn't valid");
        }

        const {name, power, defense, health = 100} = req.body;
        
        if(!isName(name)){
            throw new Error("Name is incorrect")
        }

        if(!isPower(power)){
            throw new Error("Power must be between 1 and 100")
        }

        if (!isDefense(defense)) {
            throw new Error("Defense must be between 1 and 10")
        }

        if (!isHealth(health)) {
            throw new Error("Health must be between 80 and 120")            
        }
        
        next();
    } catch (err) {
       res.status(400).send(JSON.stringify({error: true, message: err.message}))
    }

}

const updateFighterValid = (req, res, next) => {
    try {
        if(!req.body){
            throw new Error('Request body must be not null')
        }

        const correctProps = checkOnUpdateModel(fighter, req.body);
        
        if (!correctProps || req.body.hasOwnProperty("id")){
            throw new Error("Fighter entity to update isn't valid");
        }

        const {name, power, defense, health} = req.body;
        
        if(name && !isName(name)){
            throw new Error("Name is incorrect")
        }

        if(power && !isPower(power)){
            throw new Error("Power must be between 1 and 100")
        }

        if (defense && !isDefense(defense)) {
            throw new Error("Defense must be between 1 and 10")
        }

        if (health && !isHealth(health)) {
            throw new Error("Health must be between 80 and 120")            
        }
        
        next();
    } catch (err) {
       res.status(400).send(JSON.stringify({error: true, message: err.message}))
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;