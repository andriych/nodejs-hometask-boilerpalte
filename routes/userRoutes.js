const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
      const users = UserService.searchAll();
      res.data = users;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
      const user = UserService.search({id: req.params['id']});

      if (!user){
        res.status(404);
        throw new Error("User not found");
      }
      
      res.data = user;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  try {
      const email = req.body.email.toLowerCase();
      const existUser = UserService.search({email}) || UserService.search({phoneNumber: req.body.phoneNumber})
      
      if (existUser){
        res.status(400);
        throw new Error("User with such an email or phone already exists");
      }

      const user = UserService.create({...req.body, email});
      res.data = user;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
      const user = UserService.search({id: req.params['id']});

      if (!user){
        res.status(404);
        throw new Error("User not found");
      }

      const existUser = UserService.search({email: req.body.email?.toLowerCase() || ""}) 
                        || UserService.search({phoneNumber: req.body.phoneNumber || ""})
      
      if (existUser && existUser.id !== req.params['id']){
        res.status(400);
        throw new Error("User with such an email or phone already exists");
      }

      const updatedUser = UserService.update(req.params['id'], req.body);
      res.data = updatedUser;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
      const user = UserService.search({id: req.params['id']});

      if (!user){
        res.status(404);
        throw new Error("User not found");
      }

      const deletedUser = UserService.delete(req.params['id']);
      res.data = deletedUser;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);


module.exports = router;