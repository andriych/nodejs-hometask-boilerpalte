const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

router.get('/', (req, res, next) => {
  try {
      const fighters = FighterService.searchAll();
      res.data = fighters;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);


router.get('/:id', (req, res, next) => {
  try {
      const fighter = FighterService.search({id: req.params['id']});
      
      if(!fighter){
        res.status(404);
        throw new Error("Fighter not found");
      }

      res.data = fighter;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.post('/', createFighterValid, (req, res, next) => {
  try {
      const allFighters = FighterService.searchAll();
      const existFighter = allFighters.some(el => {
        return el.name.toLowerCase() === req.body.name.toLowerCase()
      });

      if (existFighter){
        res.status(400);
        throw new Error("Fighter with such name already exists");
      }
      const fighter = FighterService.create(req.body);
      res.data = fighter;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  try {
      const fighter = FighterService.search({id: req.params['id']});

      if (!fighter){
        res.status(404);
        throw new Error("Fighter not found");
      }

      const existFighter = FighterService.search(req.body.name || "");
      
      if (existFighter && existFighter.id !== req.params['id']){
        res.status(400);
        throw new Error("Fighter with such name already exists");
      }

      const updatedFighter = FighterService.update(req.params['id'], req.body);
      res.data = updatedFighter;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
      const fighter = FighterService.search({id: req.params['id']});

      if (!fighter){
        res.status(404);
        throw new Error("Fighter not found");
      }

      const deletedfighter = FighterService.delete(req.params['id']);
      res.data = deletedfighter;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);


module.exports = router;