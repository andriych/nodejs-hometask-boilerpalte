const { user } = require('../models/user');

const isName = (value) => {
    if (!value) return false;
    if (!value.trim()) return false;
    return true;
}

const isEmail = (value) => {
    if (!value) return false;
    const regex = new RegExp('[a-z0-9]+@[a-z]+\.[a-z]{2,3}');
    if (!(value.trim() && regex.test(value) && value.slice(-10) === "@gmail.com")) return false;
    return true;
}

const isPhone = (value) => {
    if (!value) return false;
    const validation = value.slice(0, 4) === "+380" && value.length === 13;
    if (!(value.trim() && validation)) return false;
    return true;
}

const isPassword = (value) => {
    if (!value) return false;
    if (!(value.trim() && value.length >= 3)) return false;
    return true;
}

const checkOnCreateModel = (objOne, objTwo) => {
    const objOneKeys = Object.keys({...objOne, id: ""}).sort();
    const objTwoKeys = Object.keys({...objTwo, id: ""}).sort();
    return  JSON.stringify(objOneKeys) === JSON.stringify(objTwoKeys);
}

const checkOnUpdateModel = (pattern, toCompare) => {
    const patternKeys = Object.keys(pattern);
    const toCompareKeys = Object.keys(toCompare);

    if(patternKeys.length === 0 || toCompareKeys.length === 0) return false;

    const otherKeys = toCompareKeys.filter(a => !patternKeys.some(b => a === b))
    return !otherKeys.length;
}

const createUserValid = (req, res, next) => {
    try {
        if(!req.body){
            throw new Error('Request body must be not null')
        }
        
        const correctProps = checkOnCreateModel(user, req.body);

        if (!correctProps || req.body.hasOwnProperty("id")){
            throw new Error("User entity to create isn't valid");
        }

        const {firstName, lastName, email, phoneNumber, password} = req.body;

        if (!isName(firstName)){
            throw new Error("First name is incorrect")
        }

        if (!isName(lastName)){
            throw new Error("Last name is incorrect")
        }

        if (!isEmail(email)) {
            throw new Error("Email is incorrect")
        }

        if (!isPhone(phoneNumber)) {
            throw new Error("Phone number must be +380xxxxxxxxx")            
        }

        if (!isPassword(password)) {
            throw new Error("Password must be at least 3 characters long")
        }
 
        next();
    } catch (err) {
        res.status(400).send(JSON.stringify({error: true, message: err.message}))
    }
}

const updateUserValid = (req, res, next) => {
    try {
        if(!req.body){
            throw new Error('Request body must be not null')
        }

        const correctProps = checkOnUpdateModel(user, req.body);

        if (!correctProps || req.body.hasOwnProperty("id")){
            throw new Error("User entity to create isn't valid");
        }

        const {firstName, lastName, email, phoneNumber, password} = req.body;
        
        if (firstName && !isName(firstName)){
            throw new Error("First name is incorrect")
        }

        if (lastName && !isName(lastName)){
            throw new Error("First name is incorrect")
        }

        if (email && !isEmail(email)) {
            throw new Error("Email name is incorrect. Check if it is gmail")
        }

        if (phoneNumber && !isPhone(phoneNumber)) {
            throw new Error("Phone number must be +380xxxxxxxxx")            
        }

        if (password && !isPassword(password)) {
            throw new Error("Password must be at least 3 characters long")
        }

        next();
    } catch (err) {
        res.status(400).send(JSON.stringify({error: true, message: err.message}))
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;